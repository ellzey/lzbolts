#pragma once

#include <dirent.h>


#define fs_dir_open(dpath) opendir(dpath)
#define fs_dir_close(dptr) closedir(dptr)
#define fs_dir_isdir(dptr) dptr->d_type == DT_DIR

static inline void
fs__closedirp_(DIR ** d)
{
    if (*d) fs_dir_close(*d);
}

static inline void
fs__closep_(int * fd)
{
    if (*fd) close(*fd);
}

#define fs_DIR __attribute__((cleanup(fs__closedirp_))) DIR
#define fs_FD  __attribute__((cleanup(fs__closep_))) int

/* fs_dir_foreach(DIR *dir, struct dirent * dirent); */
#define fs_dir_foreach(dirptr__, dvar__)                                 \
    while ((dvar__ = readdir(dirptr__)) != NULL)                         \
        if (strcmp(dvar__->d_name, ".") && strcmp(dvar__->d_name, "..")) \

/* fs_dir_merge("/home/mark", "/file"); */
#define fs_dir_merge(base, name, out)              \
    snprintf(out, sizeof(out) - 1,                 \
             (base[0] == '/' && base[1] == '\0') ? \
             "%s%s" : "%s/%s", base, name)

