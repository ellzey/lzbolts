#pragma once

#define align(d, a) (((d) + (a - 1)) & ~(a - 1))

#define align_ptr(p, a) \
    (u_char *)(((uintptr_t)(p) + ((uintptr_t)a - 1)) & ~((uintptr_t)a - 1))

