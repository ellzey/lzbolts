#pragma once

static inline void
s__closedir_(DIR ** d)
{
    if (*d)
    {
        closedir(*d);
    }
}

static inline void
s__closefd_(int * fd)
{
    if (*fd)
    {
        close(*fd);
    }
}

#define s_DIR __attribute__((cleanup(s__closedir_))) DIR
#define s_FD  __attribute__((cleanup(s__closefd_))) int
