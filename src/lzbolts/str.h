#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <errno.h>
#include <assert.h>

struct string_ {
    size_t length;
    size_t alloc;
    char   buf[];
};

#define STRING_MINSIZE (1024 * 1024)

#define string_upcast(DAT) \
    (struct string_ *)((char *)(DAT - offsetof(struct string_, buf)))

static size_t
string_length(const char * s)
{
    struct string_ * str = string_upcast(s);

    return str->length;
}

static size_t
string_alloc(const char * s)
{
    struct string_ * str = string_upcast(s);

    return str->alloc - str->length;
}

static void
string_set_length(char * s, size_t len)
{
    struct string_ * str = string_upcast(s);

    str->length = len;
}

static void
string_set_alloc(char * s, size_t len)
{
    struct string_ * str = string_upcast(s);

    str->alloc = len;
}

static char *
string_newlen(const char * s, size_t len)
{
    struct string_ * str;

    if (!(str = malloc(sizeof(*str) + len + 1)))
    {
        return NULL;
    }

    str->length = str->alloc = len;

    if (len != 0)
    {
        memcpy(str->buf, s, len);
    }

    str->buf[len] = '\0';

    return str->buf;
}

static char *
string_new(const char * s)
{
    return string_newlen(s, s ? strlen(s) : 0);
}

static char *
string_empty(void)
{
    return string_newlen("", 0);
}

static char *
string_dup(const char * s)
{
    return string_newlen(s, string_length(s));
}

static void
string_free(char * s)
{
    if (s == NULL)
    {
        return;
    }

    free(string_upcast(s));
}

static void
string_tolower(char * s)
{
    size_t len = string_length(s);
    size_t i;

    for (i = 0; i < len; i++)
    {
        s[i] = tolower(s[i]);
    }
}

static void
string_toupper(char * s)
{
    size_t len = string_length(s);
    size_t i;

    for (i = 0; i < len; i++)
    {
        s[i] = toupper(s[i]);
    }
}

static char *
string_make_room(char * s, size_t add_len)
{
    size_t alloc = string_alloc(s);
    size_t len   = string_length(s);
    size_t new_len;

    if (alloc > add_len)
    {
        return s;
    }

    new_len = len + add_len;

    if (new_len < STRING_MINSIZE)
    {
        new_len *= 2;
    } else {
        new_len += STRING_MINSIZE;
    }

    {
        struct string_ * str = string_upcast(s);
        struct string_ * strn;

        strn = realloc(str, sizeof(*str) + new_len + 1);
        s    = strn->buf;
    }


    string_set_alloc(s, new_len);

    return s;
}

static char *
string_catlen(char * s, const char * in, const size_t len)
{
    size_t curr_len = string_length(s);

    s = string_make_room(s, len);

    memcpy(s + curr_len, in, len);
    s[curr_len + len] = '\0';

    string_set_length(s, curr_len + len);
    return s;
}

static char *
string_cat(char * s, const char * in)
{
    return string_catlen(s, in, strlen(in));
}

static char *
string_copylen(char * s, const char * in, const size_t len)
{
    if (string_alloc(s) < len)
    {
        s = string_make_room(s, len - string_length(s));
    }

    memcpy(s, in, len);
    s[len] = '\0';

    string_set_length(s, len);
    return s;
}

static char *
string_copy(char * s, const char * in)
{
    return string_copylen(s, in, strlen(in));
}

static char **
string_split(const char * s, const char * sep, int * count)
{
    int     elements = 0;
    int     slots    = 5;
    int     start    = 0;
    int     i;
    char ** tokens;
    size_t  str_len  = string_length(s);
    size_t  sep_len  = strlen(sep);

    tokens = malloc(sizeof(struct string_ *) * slots);

    for (i = 0; i < (str_len - (sep_len - 1)); i++)
    {
        if (slots < elements + 2)
        {
            char ** newtokens;

            slots    *= 2;
            newtokens = realloc(tokens, sizeof(struct string_ *) * slots);

            tokens    = newtokens;
        }

        if (memcmp(s + i, sep, sep_len) == 0)
        {
            tokens[elements++] = string_newlen(s + start, i - start);

            start = i + sep_len;
            i += sep_len - 1;
        }
    }


    tokens[elements++] = string_newlen(s + start, str_len - start);
    *count = elements;

    return tokens;
}

static char *
string_join(char ** argv, int argc, char * sep)
{
    char * joined = string_empty();
    int    i;

    for (i = 0; i < argc; i++)
    {
        joined = string_cat(joined, argv[i]);

        if (i != argc - 1)
        {
            joined = string_cat(joined, sep);
        }
    }

    return joined;
}

static char *
string_trim(char * s, const char * trims)
{
    char * start;
    char * end;
    char * sp;
    char * ep;
    size_t len;

    sp = start = s;
    ep = end = s + string_length(s) - 1;

    while (sp <= end && strchr(trims, *sp))
    {
        sp++;
    }

    while (ep > sp && strchr(trims, *ep))
    {
        ep--;
    }

    len = (sp > ep) ? 0 : ((ep - sp) + 1);

    if (s != sp)
    {
        memmove(s, sp, len);
    }

    s[len] = '\0';

    string_set_length(s, len);

    return s;
}
