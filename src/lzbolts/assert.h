#pragma once

#define affirm(x)                                                     \
    do {                                                              \
        if (unlikely(!(x)))                                           \
        {                                                             \
            fprintf(stderr, "Assertion failed: %s (%s:%s:%d)\n", # x, \
                    __func__, __FILE__, __LINE__);                    \
            fflush(stderr);                                           \
            abort();                                                  \
        }                                                             \
    } while (0)

#define alloc_affirm(x)                                   \
    do {                                                  \
        if (unlikely(!x))                                 \
        {                                                 \
            fprintf(stderr, "Out of memory (%s:%s:%d)\n", \
                    __func__, __FILE__, __LINE__);        \
            fflush(stderr);                               \
            abort();                                      \
        }                                                 \
    } while (0)

#define affirm_fmt(x, fmt, ...)                                          \
    do {                                                                 \
        if (unlikely(!(x)))                                              \
        {                                                                \
            fprintf(stderr, "Assertion failed: %s (%s:%s:%d) " fmt "\n", \
                    # x, __func__, __FILE__, __LINE__, __VA_ARGS__);     \
            fflush(stderr);                                              \
            abort();                                                     \
        }                                                                \
    } while (0)

#define errno_affirm(x)                             \
    do {                                            \
        if (unlikely(!(x)))                         \
        {                                           \
            fprintf(stderr, "%s [%d] (%s:%s:%d)\n", \
                    strerror(errno), errno,         \
                    __func__, __FILE__, __LINE__);  \
            fflush(stderr);                         \
            abort();                                \
        }                                           \
    } while (0)


