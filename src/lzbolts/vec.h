#include <stdlib.h>
#include <string.h>
#include <bolts/sfree.h>

#define vec_t(T)      \
    struct {          \
        T * data;     \
        int length;   \
        int capacity; \
    }

static int
vec__expand_(char ** data,
             int   * length,
             int   * capacity,
             int     memsz)
{
    if (*length + 1 > *capacity)
    {
        void * ptr;
        int    n = (*capacity == 0) ? 1 : *capacity << 1;

        if ((ptr = realloc(*data, n * memsz)) == NULL)
        {
            return -1;
        }

        *data     = ptr;
        *capacity = n;
    }

    return 0;
}

static int
vec__reserve_(char ** data,
              int   * length,
              int   * capacity,
              int     memsz,
              int     n)
{
    void * ptr;

    if (n > *capacity)
    {
        if ((ptr = realloc(*data, n * memsz)) == NULL)
        {
            return -1;
        }


        *data     = ptr;
        *capacity = n;
    }

    return 0;
}

static int
vec__reserve_po2_(char ** data,
                  int   * length,
                  int   * capacity,
                  int     memsz,
                  int     n)
{
    int n2 = 1;

    if (n == 0)
    {
        return 0;
    }

    while (n2 < n)
    {
        n2 <<= 1;
    }

    return vec__reserve_(data, length, capacity, memsz, n2);
}

static int
vec__insert_(char ** data,
             int   * length,
             int   * capacity,
             int     memsz,
             int     idx)
{
    if (vec__expand_(data, length, capacity, memsz) == -1)
    {
        return -1;
    }


    memmove(*data + (idx + 1) * memsz,
            *data + idx * memsz,
            (*length - idx) * memsz);

    return 0;
}

static int
vec__splice_(char ** data,
             int   * length,
             int   * capacity,
             int     memsz,
             int     start,
             int     count)
{
    memmove(*data + start * memsz,
            *data + (start + count) * memsz,
            (*length - start - count) * memsz);
}

#define vec_unpack_(v)                 \
    (char **)&(v)->data, &(v)->length, \
    &(v)->capacity, sizeof(*(v)->data)

#define vec_push(v, val)                         \
    (                                            \
        vec__expand_(vec_unpack_(v)) ? -1 :      \
        ((v)->data[(v)->length++] = (val), 0), 0 \
    )

#define vec_splice(v, start, count)                 \
    (                                               \
        vec__splice_(vec_unpack_(v), start, count), \
        (v)->length -= (count)                      \
    )


#define vec_swapsplice(v, start, count)                 \
    (                                                   \
        vec__swapsplice_(vec_unpack_(v), start, count), \
        (v)->length -= (count)                          \
    )


#define vec_insert(v, idx, val)                       \
    (                                                 \
        vec__insert_(vec_unpack_(v), idx) ? -1 :      \
        ((v)->data[idx] = (val), 0), (v)->length++, 0 \
    )


#define vec_init(v)       memset((v), 0, sizeof(*(v)))
#define vec_clear(v)      ((v)->length = 0)
#define vec_first(v)      (v)->data[0]
#define vec_last(v)       (v)->data[(v)->length - 1]
#define vec_reserve(v, n) vec__reserve_(vec_unpack_(v), n)
#define vec_pop(v)        (v)->data[--(v)->length]
#define vec_length(v)     (v)->length

#define vec_deinit(v)               \
    (                               \
        safe_free((v)->data, free), \
        vec_init(v)                 \
    )

#define vec_pusharr(v, arr, count)                                     \
    do {                                                               \
        int i__, n__ = (count);                                        \
                                                                       \
        if (vec__reserve_po2_(vec_unpack_(v), (v)->length + n__) != 0) \
        {                                                              \
            break;                                                     \
        }                                                              \
                                                                       \
        for (i__ = 0; i__ < n__; i__++)                                \
        {                                                              \
            (v)->data[(v)->length++] = (arr)[i__];                     \
        }                                                              \
                                                                       \
    } while (0)


#define vec_extend(v, v2) \
    vec_pusharr((v), (v2)->data, (v2)->length)

#define vec_find(v, val, idx)                         \
    do {                                              \
        for ((idx) = 0; (idx) < (v)->length; (idx)++) \
        {                                             \
            if ((v)->data[(idx)] == (val))            \
            {                                         \
                break;                                \
            }                                         \
        }                                             \
                                                      \
        if ((idx) == (v)->length)                     \
        {                                             \
            (idx) = -1;                               \
        }                                             \
                                                      \
    } while (0)


#define vec_remove(v, val)           \
    do {                             \
        int idx__;                   \
                                     \
        vec_find(v, val, idx__);     \
                                     \
        if (idx__ != -1)             \
        {                            \
            vec_splice(v, idx__, 1); \
        }                            \
    } while (0)


#define vec_reverse(v)                                   \
    do {                                                 \
        int i__ = (v)->length / 2;                       \
        while (i__--)                                    \
        {                                                \
            vec_swap((v), i__, (v)->length - (i__ + 1)); \
        }                                                \
    } while (0)


#define vec_foreach(v, var, iter)                                      \
    if ((v)->length > 0)                                               \
        for ((iter) = 0;                                               \
             (iter) < (v)->length && (((var) = (v)->data[(iter)]), 1); \
             ++(iter))


#define vec_foreach_rev(v, var, iter)                         \
    if ((v)->length > 0)                                      \
        for ((iter) = (v)->length - 1;                        \
             (iter) >= 0 && (((var) = (v)->data[(iter)]), 1); \
             --(iter))


#define vec_foreach_ptr(v, var, iter)                                   \
    if ((v)->length > 0)                                                \
        for ((iter) = 0;                                                \
             (iter) < (v)->length && (((var) = &(v)->data[(iter)]), 1); \
             ++(iter))


#define vec_foreach_ptr_rev(v, var, iter)                      \
    if ((v)->length > 0)                                       \
        for ((iter) = (v)->length - 1;                         \
             (iter) >= 0 && (((var) = &(v)->data[(iter)]), 1); \
             --(iter))



