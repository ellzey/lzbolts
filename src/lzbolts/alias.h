#pragma once

#define alias(_name, _aliasname) \
    __typeof(_name) _aliasname __attribute__((alias(# _name)))

