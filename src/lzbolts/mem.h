#pragma once

#define safe_free(_var, _freefn) do { \
        _freefn((_var));              \
        (_var) = NULL;                \
}  while (0)


